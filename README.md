# What Personal Data do we collect from you and what do we do with it? #
We collect you Personal Data when you choose to upload a trip for emission analysis. The collected data includes:

* Your GPS coordinates (longitude, latitude, altitude) registered by your iPhone during the trip.
* Time stamp for each registered coordinate.
* Your stated means of transportation.
* Your stated organization belonging.
* A unique identifier, based on your iPhone.

The data are stored in Microsoft Azure (servers in Northern Ireland) and the data processing/visualization is done Qlik Enterprise without any third-party access. 

# Why do we collect your Personal Data? #
The purpose of Combitech´s processing of your Personal Data is to provide customized CO2 calculation for your organization. We collect your Personal Data to give your organization insights regarding their members travel habits, including common traveling routes, seasonal patterns, choice transportation means and the number of unique users within the analysis etc. 

# Who might we share your Personal Data with? #
Your Personal Data will be used by Combitech and will not be shared with a third party. 

# What are your rights? # 
If your Personal Data are incorrect or needs to be updated, you may at any time request that we correct or update the Personal Data by contacting the Data-processor. You may also contact us if you no longer wish that we process your Personal Data, if you prefer us to restrict our processing in any manner or if you no longer wish to receive information about Combitech. We will then delete your Personal Data from our systems or restrict our processing of your Personal Data. [Please note however, that an erasure of your Personal Data or a restriction of our processing of your Personal Data may mean that we will not be able to provide our services to you, wholly or partially]. In addition, you may receive a copy of the Personal Data relating to you and information regarding our processing of such personal data by applying to the Data-processor. In such case, we will provide your Personal Data to you in a commonly used data format. 
If you have any queries regarding the processing of your Personal Data or wish to exercise any of the rights stated above, please write to the Data-processor at the address provided below.  You have the right to lodge a complaint regarding how Combitech processes your Personal Data to the relevant data protection authority or similar body within your jurisdiction. 



# How can you contact the Data-processor and exercise your rights? #
The Data-processor of any Personal Data we hold about you is Combitech AB, corporate identity 556218-6790, Universitetsvägen 14, P.O Box 15042, SE-580 15 Linköping, Sweden. You can contact the Sub-processor at the following contact information: info@combitech.se Combitech is a company in the Saab Group.

